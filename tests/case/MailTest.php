<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web;

use Nora;

class MailTest extends \PHPUnit_Framework_TestCase
{
    public function testMail ( )
    {

        Nora::Configure_write('mail.smtp.host', 'tcp://smtp-gw:25');
        Nora::Configure_write('mail.from', 'hajime@nora-worker.net');

        Nora::environment_register();

        Nora::logging_addLogger([
            'writer' => 'mail://hajime.matsumoto@avap.co.jp;subject=えらー2'
        ]);
        Nora::logging_addLogger([
            'writer' => 'file:///tmp/a'
        ]);

        Nora::logNotice('ほげ');
        Nora::logNotice('あいうえお');
        Nora::logNotice('かきくけこ');
        Nora::logNotice('さしすせそ');

        die();

    

        // メール送信テスト
        /*
        $smtp = Nora::mail_smtp();
        $smtp->mailfrom('hajime@nora-worker.net');
        $smtp->rcptTo('hajime@nora-worker.net');
        $smtp->dataStart();
        $smtp->data('Subject: 222222');
        $smtp->data('From: hajime<hajime@nora-worker.net>');
        $smtp->data();
        $smtp->data('test');
        $smtp->dataEnd();
         */

        return Nora::module('mail');
    }

    /**
     * @depends testMail
     */
    public function testParse($module)
    {
        $mail_file = TEST_PROJECT_PATH.'/sample.mail';

        $mail = Nora::mail_parse($mail_file);

        // 添付ファイルを埋め込んだHTMLを生成
        file_put_contents('a.html', $mail->toHtml());


        // 普通メールを作成する
        $mail = $module
            ->mail( )
            ->subject('あいうえおあいうえおあいうえおあいうえおあいうえおあいうえお')
            ->from('hajime@nora-worker.net', '松本　創')
            ->to('hajime.matsumoto@avap.co.jp', 'まつもと　はじめ')
            ->Cc('game.hajime.matsumoto@gmail.com', 'まつもと　はじめ')
            ->Bcc('hajime@nora-worker.net', 'まつもと　はじめ')
            ->html(file_get_contents('a.html'))
            ;

        $module->send($mail);
        
        // オルタナティブにする
        $mail = $module
            ->mail( )
            ->subject('オルタナティブ')
            ->from('hajime@nora-worker.net')
            ->to('hajime@nora-worker.net')
            ->alternative(
                [
                    'type' => 'plain',
                    'body' => 'テキスト'
                ],
                [
                    'type' => 'html',
                    'body' => file_get_contents('a.html'),
                ]
            );

        $module->send($mail);
        
        // マルチパートミクスドメールを送る
        $mail = $module
            ->mail( )
            ->subject('マルチパートミクスド')
            ->from('hajime@nora-worker.net')
            ->to('hajime@nora-worker.net')
            ->mixed(
                [
                    'type' => 'alternative',
                    'parts' => [
                        [
                            'type' => 'plain',
                            'body' => 'テキスト'
                        ],
                        [
                            'type' => 'html',
                            'body' => file_get_contents('a.html')
                        ]
                    ]
                ],
                [
                    'type' => 'attachment',
                    'body' => file_get_contents(__FILE__),
                    'name' => 'php',
                    'content-type' => 'text/php; charset=utf8'
                ],
                [
                    'type' => 'inline',
                    'body' => file_get_contents(__FILE__),
                    'id' => 'hoge',
                    'name' => 'php',
                    'content-type' => 'text/php; charset=utf8'
                ]
            );


        $module->send($mail);

        $mail = $module->parse($mail_file);

        // 宛先を変えて送信
        $module->send($mail, 'hajime@nora-worker.net', 'hajime@nora-worker.net');
    }


}
