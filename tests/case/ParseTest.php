<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web;

use Nora;

class ParseTest extends \PHPUnit_Framework_TestCase
{
    public function testMail ( )
    {
        Nora::configure()->write('mail.smtp.host', 'tcp://smtp-gw:25');

        $mail_dir = TEST_PROJECT_PATH.'/posts_from_mail';

        foreach(glob($mail_dir.'/*') as $f)
        {
            try
            {
                var_dump($f);
                $m = Nora::mail_parse($f);
                var_Dump($m->getSubject(true));
                var_Dump($m->getFrom(true));
                var_Dump($m->getTo(true));
                var_Dump($m->getDate(true));
                var_Dump(substr($m->getMailBody(true),0, 256));
                var_Dump($m->status());

                // 転送
                // $m->from('hajime@nora-worker.net');
                // Nora::mail_send($m, 'hajime.matsumoto@avap.co.jp', 'hajime@nora-worker.net');
            }catch(\Exception $e){
                echo $e;
                echo $f;
            }

            // メモリ解放
            unset($m);
                
            flush();
            ob_flush();
        }


    }


}
