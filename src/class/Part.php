<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Mail\Mail;

/**
 * Mailの構造体
 */
class Part
{
    private $body        = '';
    private $headers     = [];

    public function addHeader($field, $value)
    {
        $this->headers[$field] = $value;
        return $this;
    }

    public function getHeader($field)
    {
        foreach($this->headers as $k => $v)
        {
            if (strtolower($k) === strtolower($field))
            {
                return $v;
            }
        }
    }

}
