<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Mail\SMTP;

use Nora\Core\Component\Componentable;

/**
 * SMTP Transporter
 *
 * @author     Hajime MATSUMOTO <hajime.matsumoto@avap.co.jp>
 * @copyright  Since 20014 Nora Project
 * @license    http://nora.avap.co.jp/license.txt
 * @version    $Id:$
 */
class Transport
{
    use Componentable;

    private $socket;
    private $addr;
    private $logHandler = null;

    /**
     * Create Transport
     * 
     * @param string $host
     * @param int $port
     */
    public function __construct (  )
    {
    }

    protected function initComponentImpl (  )
    {
    }
    /**
     * Connect
     */
    public function connect ( $host, $helo = 'localhost', $crypt = false )
    {
        $client = $this->network_client($host);
        $client->writef('HELO %s', $helo);
        $client->read( );

        if ($crypt === true)
        {
            $client->crypt(true);
        }

        $this->socket = $client;
    }

    /**
     * Send Mail From
     *
     * @param string $mail
     * @return string
     */
    public function mailFrom ( $mail )
    {
        $this->socket->writef('Mail From: %s', $mail);
        return $this->socket->read();
    }

    /**
     * Send RCTPT To
     *
     * @param string $mail
     * @return string
     */
    public function rcptTo ( $mail )
    {
        $this->socket->writef('RCPT TO: %s', $mail);
        return $this->socket->read();
    }

    /**
     * Send Simple Text
     *
     * @param string $text
     */
    public function data($text = '')
    {
        $this->socket->write($text);
    }

    /**
     * Send DATA
     *
     * @return string
     */
    public function dataStart (  )
    {
        $this->socket->writef('DATA');
        return $this->socket->read();
    }

    /**
     * Send DATA End Signal
     *
     * @return string
     */
    public function dataEnd (  )
    {
        $this->socket->writef(".");
        return $this->socket->read();
    }

    /**
     * Submit Mail
     *
     */
    public function submit ( Mail $mail )
    {
        $this->connect( );
        $this->mailFrom($mail->mailFrom);
        array_walk($mail->rcptTo, function($rcpt) use ($mail) {
            $this->rcptTo($rcpt);
        });
        $this->dataStart();
        $this->data($mail->toString());
        $this->dataEnd();

    }
}
