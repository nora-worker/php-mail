<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Mail;

use Nora\Module\Mail\Parser;
use function Nora\__;

/**
 * Mailの構造体
 */
class Mail extends Mail\Part
{
    private $_rcpt_list       = [];
    private $_mail_from       = '';
    private $_header_encoding = 'ISO-2022-JP';

    /**
     * 件名を取得する
     */
    public function decodeSubject ( )
    {
        return Parser::mimeHeaderDecode(
            $this->getHeaderRaw('Subject')
        );
    }

    /**
     * 件名ヘッダを設定する
     */
    public function subject($subject)
    {
        $this->addHeader('Subject', mb_encode_mimeheader(
            $subject,
            $this->_header_encoding
        ));
        return $this;
    }

    /**
     * Fromヘッダを設定する
     */
    public function from($email, $name = null)
    {
        $this->addHeader('From', $this->convertAddress($name, $email));
        $this->mailfrom($email);
        return $this;
    }

    public function mailfrom($email)
    {
        $this->_mail_from = $email;
        return $this;
    }

    public function getMailFrom( )
    {
        return $this->_mail_from;
    }

    public function getRcptTo( )
    {
        return $this->_rcptTo_list;
    }

    /**
     * Toヘッダを設定する
     */
    public function to ($email, $name = null)
    {
        $this->addHeader('To', $this->convertAddress($name, $email));
        $this->rcptTo($email);
        return $this;
    }

    /**
     * Ccヘッダを設定する
     */
    public function cc ($email, $name = null)
    {
        $this->addHeader('Cc', $this->convertAddress($name, $email));
        $this->rcptTo($email);
        return $this;
    }

    /**
     * Bccを設定する
     */
    public function Bcc ($email)
    {
        $this->rcptTo($email);
        return $this;
    }

    /**
     * 実際の送信先を設定する
     */
    public function rcptTo($email)
    {
        $this->_rcptTo_list[] = $email;
        return $this;
    }

    /**
     * Fromを取得する
     */
    public function getFrom($decode = false)
    {
        $from = $this->getHeaderRaw('From');

        if (preg_match('/(.+)<(.+)>/', $from, $m))
        {
            $name=$m[1];
            $mail=$m[2];
        }else{
            $name=null;
            $mail=$from;
        }

        $mail=trim(trim($mail), '><');

        if ($decode === true)
        {
            $name=mb_decode_mimeheader($name);
            return $name."<$mail>";
        }
        return $mail;
    }

    /**
     * Fromを取得する
     */
    public function getTo($decode = false)
    {
        $to = $this->getHeaderRaw('To');

        if (preg_match('/(.+)<(.+)>/', $to, $m))
        {
            $name=$m[1];
            $mail=$m[2];
        }else{
            $name=null;
            $mail=$to;
        }

        $mail=trim(trim($mail), '><');

        if ($decode === true)
        {
            $name=mb_decode_mimeheader($name);
            return $name."<$mail>";
        }
        return $mail;
    }

    public function getDate($decode = false)
    {
        return $this->getHeaderRaw('date');
    }


    private function convertAddress($name, $email)
    {
        if (empty($name)) return $email;
        return sprintf( "%s <%s>",
            mb_encode_mimeheader( $name, $this->_header_encoding ),
            $email
        );
    }

    /**
     * メールをHTMLに変換する
     */
    public function toHtml()
    {
        $result = $this->searchPartByContentType('text/html');

        if ($result === false)
        {
            $result = $this->searchPartByContentType('text/plain');
            if ($result === false)
            {
                $html = $this->getBody();
            }
            else
            {
                $html = $result[0]->getBody();
            }
            $html = nl2br($html);
        }else{
            $html = $result[0]->getBody();

            // cid:xxx を変換する
            $html = preg_replace_callback('/cid:([^"\'\s]+)/', function($m) {
                $result = $this->searchPart(function($part) use ($m) {
                    return $m[1] === trim($part->getHeader('Content-ID'),'<>');
                });
                if ($result !== false)
                {
                    $part = $result[0];
                    return sprintf(
                        'data:%s;base64,%s',
                        $part->getHeader('content-type'),
                        base64_encode($part->getBody())
                    );
                }
            }, $html);

            // 添付ファイル
            foreach($this->getAttachParts() as $p)
            {
                $html.=sprintf("<img src='data:%s;base64,%s'>",
                    $p->getHeaders('content-type'),
                    base64_encode($p->getBody())
                );
            }
        }
        return $html;
    }

    /**
     * 添付ファイルを取得する
     */
    public function getAttachParts( )
    {
        $result = $this->searchPart(function($part) {
            return 0 === stripos($part->getHeader('Content-Disposition'),'attachment');
        });

        return $result;
    }

    public function getSubject($decode = false)
    {
        if ($decode === true)
        {
            return mb_decode_mimeheader($this->getHeader('Subject'));
        }
        return $this->getHeader('Subject');
    }

    public function getMailBody ( )
    {
        $res = false;
        if (false !== $parts = $this->searchPartByContentType('text/html'))
        {
            $res = $parts[0];
        }elseif (false !== $parts = $this->searchPartByContentType('text/plain'))
        {
            $res = $parts[0];
        }
        if (false === $res || false === $res->getBody())
        {
            return false;
        }
        return $res->getBody();
    }
}
