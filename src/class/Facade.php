<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Mail;

use Nora\Core\Module\Module;
use Nora\Core\Util\Collection\Hash;

/**
 * Mailモジュール
 */
class Facade extends Module
{
    protected function initModuleImpl( )
    {
    }


    /**
     * SMTPサーバを作成する
     */
    public function smtp ($host = null)
    {
        if ($host === null) $host = $this->configure_read('mail.smtp.host');

        $transport = new SMTP\Transport( );
        $transport->setScope($this->newScope());
        $transport->connect($host);

        return $transport;
    }

    /**
     * メールデータをパースする
     */
    public function parse($mail)
    {
        if (is_file($mail)) {
            $mail = file_get_contents($mail);
        }
        return Parser::parse($mail);
    }

    /**
     * メールデータを作成する
     */
    public function mail ( )
    {
        $mail = new Mail( );
        $mail->from($this->configure_read('mail.from'));
        return $mail;
    }

    /**
     *
     */
    public function plain($subject, $body, $to, $from = null, $headers =[])
    {
        $mail = $this->mail();
        $mail->subject($subject);
        $mail->plain($body);
        $mail->to($to);
        $mail->from($from === null ? $this->configure_read('mail.from'): $from);
        $mail->addHeader($headers);

        $this->send($mail);
    }

    /**
     * メールを送信する
     */
    public function send ($mail, $rcptTo = null, $mailFrom = null)
    {
        if (is_string($rcptTo)) $rcptTo = [$rcptTo];
        if ($rcptTo === null) $rcptTo = $mail->getRcptTo();
        if ($mailFrom === null) $mailFrom = $mail->getMailFrom();
        if ($mailFrom === null) $mailFrom = $this->configure_read('mail.from');


        $smtp = $this->smtp();
        $smtp->mailfrom($mailFrom);
        foreach($rcptTo as $to)
        {
            $smtp->rcptTo($to);
        }
        $smtp->dataStart();
        $smtp->data((string) $mail);
        $smtp->dataEnd();
    }
}
