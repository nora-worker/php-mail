<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Mail;

/**
 * メールパーサー
 */

class Parser 
{
    static public function parse($mail)
    {
        $part = static::parsePart($mail);
        return $part->toMail();
    }

    private static function parsePart($text)
    {
        list($headers, $body) = static::parseHeaderBody($text);


        $part = new Mail\Part();
        foreach($headers as $k=>$v) $part->addHeader($k, $v);

        if ($part->isMultiPart($boundary))
        {
            foreach(
                static::parseMultiPart($boundary, $body)
                as $child
            ) {
                $part->addPart($child);
            }
        }else{
            $part->setBody($part->decode($body));
        }

        return $part;
    }

    private static function parseMultiPart($boundary, $body)
    {
        $start = '--'.$boundary;
        $end   = '--'.$boundary.'--';

        $lines = preg_split("/(\r\n|\r|\n)/", $body);

        $started = false;
        $start_text = "";
        $count=-1;
        $parts = [];
        for($i=0;$i<count($lines);$i++)
        {
            $line = $lines[$i];

            if (trim($line) === $end)
            {
                yield static::parsePart($parts[$count]);
                break;
            }

            if (trim($line) === $start)
            {
                if ($count > -1)
                {
                    yield static::parsePart($parts[$count]);
                }
                $count++;
                continue;
            }

            if ($count === -1)
            {
                $start_text .= $line;
                continue;
            }

            if (isset($parts[$count]))
            {
                $parts[$count] .= "\n".$line;
            }else{
                $parts[$count] = $line;
            }
        }
    }
    public static function mimeHeaderDecode ($text)
    {
        foreach(imap_mime_header_decode($text) as $elem) {
            $charset = ($elem->charset == 'default') ? 'US-ASCII' : $elem->charset;
            $ret .=  iconv($charset, "UTF-8//TRANSLIT", $elem->text);
        }
        return $ret;
    }

    private static function parseHeaderBody ($mail)
    {
        // 改行コードのノーマライズ
        $lines = preg_split("/(\r\n|\r|\n)/", $mail);

        $isHeader = true;

        $body = "";
        $headers = [];
        $field = null;
        $value = null;

        for($i=0;$i<count($lines);$i++)
        {
            $line = $lines[$i];

            if ($isHeader === true && static::isEmpty($line))
            {
                $isHeader = false;
                $headers[$field] = $value;
                continue;
            }

            if ($isHeader) {
                if (static::isLineStartingWithPrintableChar($line))
                {
                    if(!preg_match('/^([^\s:]+): ?(.*)$/', $line, $matches))
                    {
                        continue;
                    }

                    if ($field !== null)
                    {
                        $headers[$field] = $value;
                    }

                    $field = null;
                    $value = null;

                    $field = $matches[1];
                    $value = $matches[2];
                }else{
                    $value.= "\n".$line;
                }
                continue;
            }

            if (empty($body))
            {
                $body = $line;
            }else{
                $body.= "\n".$line;
            }
        }
        return [
            $headers,
            $body
        ];
    }

    /**
     * 空行のチェック
     */
    static private function isEmpty($line)
    {
        $line = trim($line);
        return strlen($line) === 0;
    }

    /**
     * 継続行のチェック
     */
    static private function isLineStartingWithPrintableChar($line)
    {
        return preg_match('/^[A-Za-z]/', $line);
    }
}
